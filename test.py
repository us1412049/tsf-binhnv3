# -*- coding: utf-8 -*-
import io
import os
import shutil
import codecs

import tensorflow as tf
from tensorflow.python.saved_model import tag_constants
import numpy as np

word2id = {}
id2word = []

with io.open('data/cp.syms', encoding='utf-8') as fread:
	for line in fread:
		line = line.strip().split("\t")
		word2id[line[0]] = int(line[1])
		id2word.append(line[0])

graph = tf.Graph()
with graph.as_default():
	with tf.Session() as sess:
		tf.saved_model.loader.load(
			sess,
			[tag_constants.SERVING],
			'model/',
		)
		X = graph.get_tensor_by_name('X:0')
		y_pred = graph.get_tensor_by_name('y_pred:0')

		while True:
			line = input("\nin: ")
			if line == ".":
				break
				
			words =  line.strip().split(' ')
			if len(words) == 0 or len(words) > 4:
				continue
			
			iinput = []
			for i in range(0, max(0, 4 - len(words))):
				iinput.append(0)
			for w in words:
				iinput.append(word2id[w])
			print(iinput)

			x_batch = np.array(iinput)
			x_batch = x_batch.reshape(1, 4, 1)
			result = sess.run(y_pred, feed_dict={X: x_batch})
			print("predict:", id2word[result[0]])
			#print(result)


