# -*- coding: utf-8 -*-
from __future__ import print_function

import io
import os
import shutil
import codecs
import numpy as np

import tensorflow as tf
from tensorflow.contrib import rnn

#import time

ngram = 10
preword = ngram - 1
num_unigram = 7450

num_hidden = 256
num_input = 1

ntimes = 60

#pre training data
pre2 = [0] * (preword - 1)
pre3 = [0] * (preword - 2)
pre4 = [0] * (preword - 3)
pre5 = [0] * (preword - 4)
pre6 = [0] * (preword - 5)
pre7 = [0] * (preword - 6)
pre8 = [0] * (preword - 7)
pre9 = [0] * (preword - 8)

# RNN output node weights and biases
weights = {
	'out': tf.Variable(tf.random_normal([num_hidden, num_unigram]))
}
biases = {
	'out': tf.Variable(tf.random_normal([num_unigram]))
}

# tf graph input
X = tf.placeholder("float", [None, preword, num_input], name='X')
Y = tf.placeholder("float", [None, num_unigram])

def RNN(x, weights, biases):

	# Unstack to get a list of 'timesteps' tensors, each tensor has shape (batch_size, n_input)
	x = tf.unstack(x, preword, 1)

	# Build a LSTM cell
	lstm_cell = rnn.BasicLSTMCell(num_hidden)

	# Get LSTM cell output
	outputs, states = rnn.static_rnn(lstm_cell, x, dtype=tf.float32)

	# Linear activation, using rnn inner loop last output
	return tf.matmul(outputs[-1], weights['out']) + biases['out']


logits = RNN(X, weights, biases)
y_pred = tf.argmax(tf.nn.softmax(logits), 1, name='y_pred')
y_topd = tf.nn.softmax(logits, name='y_topd')
y_true = tf.argmax(Y, 1)

# Loss and optimizer
loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=Y))
train_op = tf.train.RMSPropOptimizer(learning_rate=0.0001).minimize(loss_op)
correct_pred = tf.equal(y_pred, y_true)
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initialize the variables with default values
init = tf.global_variables_initializer()
saver = tf.train.Saver()
with tf.Session() as sess:
	sess.run(init)
	for itt in range(0, ntimes):
		for ifile in range(46):
			batches = []
			prevw = []
			nextw = []
			print("Loading training data: %d" % ifile)
			with io.open('datasplit/cp%d.in' % ifile) as fread:
				nnnline = 1
				for line in fread:
					print("\r %d" % nnnline, end='\r')
					nnnline += 1
					line = [int(x) for x in line.strip().split()]
					for i in range(1, len(line)):
						if i > 8:
							prevw.append(line[i-9:i])
							nextw.append(line[i])
						if i > 7:
							prevw.append(pre9 + line[i-8:i])
							nextw.append(line[i])
						if i > 6:
							prevw.append(pre8 + line[i-7:i])
							nextw.append(line[i])
						if i > 5:
							prevw.append(pre7 + line[i-6:i])
							nextw.append(line[i])
						if i > 4:
							prevw.append(pre6 + line[i-5:i])
							nextw.append(line[i])
						if i > 3:
							prevw.append(pre5 + line[i-4:i])
							nextw.append(line[i])
						if i > 2:
							prevw.append(pre4 + line[i-3:i])
							nextw.append(line[i])
						if i > 1:
							prevw.append(pre3 + line[i-2:i])
							nextw.append(line[i])
						if i > 0:
							prevw.append(pre2 + [line[i-1]])
							nextw.append(line[i])
						if len(nextw) == 4096:							
							prevw = np.vstack(prevw)
							prevw = prevw.reshape(len(nextw), preword, num_input)
							batches.append([prevw, nextw])
							prevw = []
							nextw = []
						elif len(nextw) > 4096:
							npre = prevw[:4096]
							nnex = nextw[:4096]
							prevw = prevw[4096:]
							nextw = nextw[4096:]

							npre = np.vstack(npre)
							npre = npre.reshape(len(nnex), preword, num_input)
							batches.append([npre, nnex])

			if len(nextw) >= 0:
				prevw = np.vstack(prevw)
				prevw = prevw.reshape(len(nextw), preword, num_input)
				batches.append([prevw, nextw])
				prevw = []
				nextw = []

			print("Training data:", len(batches))
			nbb = len(batches)

			loss_list = []
			ibb = 1
			for j in range(nbb):
				X_batch = batches[j][0]
				Y_batch_encoded = []
				for y in batches[j][1]:
					word_vector = np.zeros([num_unigram], dtype=float)
					word_vector[y] = 1.0
					Y_batch_encoded.append(word_vector)
				Y_batch_encoded = np.vstack(Y_batch_encoded)
				Y_batch_encoded = Y_batch_encoded.reshape(len(batches[j][1]), num_unigram)

				_, acc, loss, onehot_pred = sess.run([train_op, accuracy, loss_op, logits], feed_dict={X: X_batch, Y: Y_batch_encoded})

				print('\r  %d/%d' % (ibb, nbb), end = '\r')
				ibb += 1				
				loss_list.append(loss)

			loss = sum(loss_list)/len(loss_list)
			print("Iteration " + str(itt) + ", Loss= " + "{:.4f}".format(loss))

		inputs = { "X": X }
		outputs = { "y_pred": y_pred, "y_topd": y_topd }

		if os.path.isdir("model/" + str(itt)):
			shutil.rmtree('model/' + str(itt))

		tf.saved_model.simple_save(sess, 'model/' + str(itt), inputs, outputs)
