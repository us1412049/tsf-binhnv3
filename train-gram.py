# -*- coding: utf-8 -*-
from __future__ import print_function

import io
import os
import shutil
import codecs
import numpy as np

import tensorflow as tf
from tensorflow.contrib import rnn

#import time

ngram = 5
preword = 4
num_unigram = 7450

num_hidden = 256
num_input = 1

batch_size = 2000
ntimes = 60

#load training data
batches = []
prev_w = []
next_w = []

tmpline = 1
with io.open('data02/2gram-s.txt', encoding='utf-8') as fread:
	for line in fread:
		if tmpline >= 50000:
			break
		tmpline += 1
		line = line.strip().split(' ')
		prev_w.append([0, 0, 0, int(line[0])])
		next_w.append(int(line[1]))
		if len(next_w) == batch_size:
			prev_w = np.vstack(prev_w)
			prev_w = prev_w.reshape(batch_size, preword, num_input)
			batches.append([prev_w, next_w])
			prev_w = []
			next_w = []

tmpline = 1
with io.open('data02/3gram-s.txt', encoding='utf-8') as fread:
	for line in fread:
		if tmpline >= 70000:
			break
		tmpline += 1
		line = line.strip().split(' ')
		prev_w.append([0, 0, int(line[0]), int(line[1])])
		next_w.append(int(line[2]))
		if len(next_w) == batch_size:
			prev_w = np.vstack(prev_w)
			prev_w = prev_w.reshape(batch_size, preword, num_input)
			batches.append([prev_w, next_w])
			prev_w = []
			next_w = []

tmpline = 1
with io.open('data02/4gram-s.txt', encoding='utf-8') as fread:
	for line in fread:
		if tmpline >= 70000:
			break
		tmpline += 1
		line = line.strip().split(' ')
		prev_w.append([0, int(line[0]), int(line[1]), int(line[2])])
		next_w.append(int(line[3]))
		if len(next_w) == batch_size:
			prev_w = np.vstack(prev_w)
			prev_w = prev_w.reshape(batch_size, preword, num_input)
			batches.append([prev_w, next_w])
			prev_w = []
			next_w = []

tmpline = 1
with io.open('data02/5gram-s.txt', encoding='utf-8') as fread:
	for line in fread:
		if tmpline >= 50000:
			break
		tmpline += 1
		line = line.strip().split(' ')
		prev_w.append([int(line[0]), int(line[1]), int(line[2]), int(line[3])])
		next_w.append(int(line[4]))
		if len(next_w) == batch_size:
			prev_w = np.vstack(prev_w)
			prev_w = prev_w.reshape(batch_size, preword, num_input)
			batches.append([prev_w, next_w])
			prev_w = []
			next_w = []

if len(next_w) > 0:
	prev_w = np.vstack(prev_w)
	prev_w = prev_w.reshape(len(next_w), preword, num_input)
	batches.append([prev_w, next_w])
	prev_w = []
	next_w = []

print("Training data:", len(batches))
nbb = len(batches)

# RNN output node weights and biases
weights = {
	'out': tf.Variable(tf.random_normal([num_hidden, num_unigram]))
}
biases = {
	'out': tf.Variable(tf.random_normal([num_unigram]))
}

# tf graph input
X = tf.placeholder("float", [None, preword, num_input], name='X')
Y = tf.placeholder("float", [None, num_unigram])

def RNN(x, weights, biases):

	# Unstack to get a list of 'timesteps' tensors, each tensor has shape (batch_size, n_input)
	x = tf.unstack(x, preword, 1)

	# Build a LSTM cell
	lstm_cell = rnn.BasicLSTMCell(num_hidden)

	# Get LSTM cell output
	outputs, states = rnn.static_rnn(lstm_cell, x, dtype=tf.float32)

	# Linear activation, using rnn inner loop last output
	return tf.matmul(outputs[-1], weights['out']) + biases['out']


logits = RNN(X, weights, biases)
y_pred = tf.argmax(tf.nn.softmax(logits), 1, name='y_pred')
y_topd = tf.nn.softmax(logits, name='y_topd')
y_true = tf.argmax(Y, 1)

# Loss and optimizer
loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=Y))
train_op = tf.train.RMSPropOptimizer(learning_rate=0.0001).minimize(loss_op)
correct_pred = tf.equal(y_pred, y_true)
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initialize the variables with default values
init = tf.global_variables_initializer()
saver = tf.train.Saver()
with tf.Session() as sess:
	sess.run(init)
	for i in range(0, ntimes):
		loss_list = []
		acc_list = []
		ibb = 1

		for j in range(nbb):
			X_batch = batches[j][0]
			Y_batch_encoded = []
			for y in batches[j][1]:
				word_vector = np.zeros([num_unigram], dtype=float)
				word_vector[y] = 1.0
				Y_batch_encoded.append(word_vector)
			Y_batch_encoded = np.vstack(Y_batch_encoded)
			Y_batch_encoded = Y_batch_encoded.reshape(len(batches[j][1]), num_unigram)

			_, acc, loss, onehot_pred = sess.run([train_op, accuracy, loss_op, logits], feed_dict={X: X_batch, Y: Y_batch_encoded})

			print('\r  %d/%d' % (ibb, nbb), end = '\r')
			ibb += 1
			
			loss_list.append(loss)
			acc_list.append(acc)

		loss = sum(loss_list)/len(loss_list)
		acc = sum(acc_list)/len(acc_list)

		print("Iteration " + str(i) + ", Loss= " + "{:.4f}".format(loss)
			  + ", Training Accuracy= " + "{:.2f}".format(acc * 100))

	inputs = { "X": X }
	outputs = { "y_pred": y_pred, "y_topd": y_topd }

	if os.path.isdir("model"):
		shutil.rmtree('model')

	tf.saved_model.simple_save(sess, 'model/', inputs, outputs)
